# gkeDeployment
1) Create a Docker File

# Build the Docker image
2) Build the image
  - Clone/pull the latest repository
  - export PROJECT_ID="$(gcloud config get-value project -q)"
  - docker build -t gcr.io/${PROJECT_ID}/starhopper-analytics:latest .
  - docker images

# Pushing the image to GCR
3) Uploading the image to Google Container Registry
  - gcloud auth configure-docker
  - docker push gcr.io/${PROJECT_ID}/starhopper-main:1.0

# Testing the container locally
4) Run the container locally
  - docker run --rm -p 8080:8080 gcr.io/${PROJECT_ID}/starhopper-analytics:latest

# Creating the cluster using gcloud command
5) Creating container Cluster
  - gcloud container clusters create starhopper --enable-autoscaling --num-nodes=2 --min-nodes 1 --max-nodes 4
  - gcloud compute instances list
  - gcloud container clusters get-credentials starhopper --zone us-central1-a

# Deploy the application in cluster
6) Deploy the application
  - kubectl run starhopper-analytics --image=gcr.io/${PROJECT_ID}/starhopper-analytics:latest --port 8080

# Exposing the application if need to expose only using Service
7) Expose the application by creating service
  - kubectl expose deployment starhopper-analytics --type=LoadBalancer --port 80 --target-port 8080

# helm charts
8) Use the below command for deployment using helm

helm upgrade --install starhopper-analytics --namespace default ./

9) To delete the deployment using helm, use the below command

helm delete --purge starhopper-analytics

# logs
10) Monitor the logs
- kubectl logs <podname>


  